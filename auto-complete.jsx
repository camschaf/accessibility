import React from 'react'

import '../css/auto-complete.css'

class AutoCompleteComponent extends React.Component {
    constructor() {
        super()
        this.state = {
            locations: ["Aberfeldy Township", "Altona", "Arthurs Creek", "Arthurs Seat", 
                        "Ashwood", "Bacchus Marsh Werribee River", "Ballan", "Beaconsfield Upper", 
                        "Beenak", "Berwick", "Blackburn", "Blackburn North", "Blue Mountain", 
                        "Box Hill", "Braeside", "Braeside Park", "Broadmeadows", "Brooklyn", 
                        "Bulla", "Bulla North", "Bulleen", "Bundoora", "Burnley", "Burwood East", 
                        "Cambarville", "Cardinia", "Caulfield", "Caulfield North", "Cement Creek", 
                        "Christmas Hills", "Clarkefield", "Clarkefield", "Clayton", "Clearwater Aqueduct", 
                        "Coburg", "Coldstream", "Collingwood", "Craigieburn", "Craigieburn East", 
                        "Cranbourne", "Dandenong", "Dandenong South", "Dandenong West", "Darraweit", 
                        "Deer Park", "Devilbend Reservoir", "Diggers Rest", "Dixons Creek", "Doncaster", 
                        "Doncaster East", "Drouin West", "Durdidwarrah", "Eastern G.C. Doncaster", 
                        "Elsternwick", "Eltham", "Emerald", "Epping", "Essendon", "Fairfield", "Fawkner", 
                        "Fiskville", "Flemington", "Footscray", "Frankston North", "Frankston Pier", 
                        "Gardiner", "Glen Forbes South", "Glen Waverley", "Graceburn", 
                        "Graceburn Creek Aqueduct", "Greensborough", "Greenvale Reservoir", "Groom's Hill", 
                        "Hampton", "Hampton Park", "Hawthorn", "Headworks", "Healesville", "Heathmont", 
                        "Heidelberg", "Hurstbridge", "Iona", "Ivanhoe", "Kangaroo Ground", "Keilor", 
                        "Keilor North", "Kew", "Keysborough", "Kinglake", "Knox", "Konagaderra", "Kooweerup", 
                        "Lake Borrie", "Lancefield", "Lancefield North", "Launching Place", "Lilydale Lake", 
                        "Little River", "Loch", "Longwarry North", "Lower Plenty", "Lyndhurst", "Lysterfield", 
                        "Maribyrnong", "Maroondah Reservoir", "Melton Reservoir", "Melton Sth Toolern Creek", 
                        "Mentone", "Mernda", "Millgrove", "Mitcham", "Montrose", "Mooroolbark", "Mornington", 
                        "Mount Dandenong", "Mount Evelyn", "Mount View", "Mt Blackwood", "Mt Bullengarook", 
                        "Mt Donna Buang", "Mt Evelyn Stringybark Creek", "Mt Gregory", "Mt Hope", 
                        "Mt Horsfall", "Mt Juliet", "Mt Macedon", "Mt St Gwinear", "Mt St Leonard", 
                        "Mt Waverley", "Myrrhee", "Narre Warren North", "Nayook", "Neerim South", 
                        "Neerim-Elton Rd", "Neerim-Neerim Creek", "Neerim-Tarago East Branch", 
                        "Neerim-Tarago West Branch", "North Wharf", "Northcote", "Notting Hill", "Nutfield", 
                        "O'Shannassy Reservoir", "Oakleigh South", "Officer", "Officer South", "Olinda", 
                        "Pakenham", "Pakenham East", "Pakenham West", "Parwon Parwan Creek", "Poley Tower", 
                        "Preston", "Reservoir", "Ringwood", "Rockbank", "Romsey", "Rosslynne Reservoir", 
                        "Rowville", "Sandringham", "Scoresby", "Seaford", "Seaford North", "Seville East", 
                        "Silvan", "Smiths Gully", "Somerton", "Southbank", "Spotswood", "Springvale", 
                        "St Albans", "St Kilda Marina", "Sunbury", "Sunshine", "Surrey Hills", 
                        "Tarago Reservoir", "Tarrawarra", "Templestowe", "The Basin", "Thomson Dam", 
                        "Tonimbuk", "Toolern Vale", "Torourrong Reservoir", "U/S Goodman Creek Lerderderg River", 
                        "Upper Lang Lang", "Upper Pakenham", "Upper Yarra Dam", "Wallaby Creek", "Wallan", 
                        "Wantirna South", "Warrandyte", "Williamstown", "Woori Yallock", "Woori Yallock Creek", 
                        "Wyndham Vale", "Yallock outflow Cora Lyn", "Yannathan", "Yarra Glen", 
                        "Yarra Glen Steels Creek", "Yarra Junction", "Yarra River downstream Doctors Creek", 
                        "Yellingbo", "Yering"],     //location list.
            suggestions: [],     //holds the autocomplete suggestions.
            counter: null        //holds the announcement text.
        }
        this.search_refs = []   //holds the dynamically created suggestion refs.

        this.autoCompleteResults = this.autoCompleteResults.bind(this)
        this.renderResults = this.renderResults.bind(this)
        this.keyPress = this.keyPress.bind(this)
        this.stopDefaultBehaviours = this.stopDefaultBehaviours.bind(this)
        this.mouseClick = this.mouseClick.bind(this)
        this.highlightSearch = this.highlightSearch.bind(this)
        this.findNextHightlight = this.findNextHightlight.bind(this)
        this.setHighlight = this.setHighlight.bind(this)
        this.clearHighlighted = this.clearHighlighted.bind(this)
        this.clearArrays = this.clearArrays.bind(this)
        this.selectOption = this.selectOption.bind(this)
        this.setSuggestions = this.setSuggestions.bind(this)
    }

    keyPress(e){    //checks what key was pressed for the search suggestion keyboard navigation.
        let highlighted = false    //used to check if a suggestion is highlighted already.
        try{
            if(this.search_refs.length > 0)    //loop through all the suggestions until you find a highlighted suggestion.   
                this.search_refs.forEach(suggestion => {
                    if(RegExp('highlight').test(suggestion.className)) highlighted = true })
        }catch(err){/*console.log('could not search for highlight')*/}

        switch(e.key){
            case 'Escape':      //escape key clears the suggestions list.
                this.stopDefaultBehaviours(e)    //prevent default behaviour of the esc key when pressed.
                this.clearHighlighted()
                break
            case 'ArrowRight':  //right arrow key assigns the value of the search box to the selected suggestion.
                if(highlighted)
                    this.selectOption()
                break
            case 'Tab':         //tab key clears the suggestions list.
                this.clearHighlighted()
                break
            case 'Enter':       //enter key assigns the value of the search box to the selected suggestion.
                if (highlighted) {
                    this.stopDefaultBehaviours(e)    //prevent input from sending data when key is pressed.
                    this.selectOption()
                }
                break
            case 'ArrowUp':     //up arrow key scrolls upwards through the suggestions list.
            this.stopDefaultBehaviours(e)    //prevent page scrolling up when key is pressed.
                if(this.search_refs.length > 0)
                    this.highlightSearch(highlighted,'up')  //logic for highlighting the suggestions.
                break
            case 'ArrowDown':   //down arrow key scrolls downwards through the suggestions list.
                this.stopDefaultBehaviours(e)    //prevent page scrolling down when key is pressed.
                if(this.search_refs.length > 0)
                    this.highlightSearch(highlighted,'down') //logic for highlighting the suggestions.
                break
            default:
        }
    }

    stopDefaultBehaviours(key){  //stops the default behaviour of the keys that the user presses.
        key.preventDefault(), key.stopPropagation() 
    }
  
    mouseClick(e){      //when the user clicks on one of the suggestions.
        let suggestion = e.target              //get the suggestion that was clicked on.
        if(suggestion){
            this.refs.search.focus()           //return focus back to the input field.
            this.selectOption(true,suggestion) //set the value in the input field to that of the suggestions text.
        }
    }

    highlightSearch(highlighted, direction){
        this.refs.search.removeAttribute('aria-activedescendant')
        if(highlighted){    //if there is currently a highlighted suggestion. find the index location of the highlighted suggestion.
            let count = 0
            let found = false
            this.search_refs.forEach(suggestion => {
                if(!found) RegExp('highlight').test(suggestion.className) ? found = true : count++ })
            this.setHighlight(count,false)  //unsets the suggestion highlight.
            try{    //try catch to catch the error when trying to access out of range index.
                this.setHighlight(this.findNextHightlight(true, direction, count),true)  //sets the suggestion highlight.
            }catch(err){/*console.log('tried to access ref that does not exist.')*/}
        }else{  //if there are no highlighted suggestions.
            this.setHighlight(this.findNextHightlight(false, direction, this.search_refs.length),true)  //sets the suggestion highlight.
        }
    }

    findNextHightlight(prev_highlight, direction, current){ //finds the next suggestion that will be highlighted.
        let next_suggestion = null     //stores the next suggestions position and is returned.
        if(direction === 'up') next_suggestion = current - 1 //if the up arrow was pressed.
        else if(direction === 'down') //if the down arrow was pressed.
            prev_highlight ? next_suggestion = current + 1 : next_suggestion = 0   //check if there is a highlighted suggestion.
        return next_suggestion         //returns the final calculated suggestion position.
    }

    setHighlight(ref_num, attr_set){     //used to set or unset the highlight attribute for the list options.
        this.search_refs[ref_num].setAttribute('aria-selected',attr_set)   //sets the aria-selected attribute to true or false.
        if(attr_set){   //checks if setting the attribute to true or false.
            this.search_refs[ref_num].setAttribute('class','suggestion highlight')  //sets the class of the selected suggestion.
            this.refs.search.setAttribute('aria-activedescendant', this.search_refs[ref_num].id)   //sets the activedescendant of the search input field.
        }else
            this.search_refs[ref_num].setAttribute('class','suggestion')       //sets the class of the deselected suggestion.
    }

    clearHighlighted(){ //used when pressing esc/right arrow or tab clear the search suggestions.
        this.refs.search.removeAttribute('aria-activedescendant')
        this.clearArrays()     //clears the suggestion and refs arrays.
        this.state.counter = null
        this.setSuggestions(this.state.suggestions, this.state.counter)  //sets the state for the 2 given state variables.
    }

    clearArrays(){      //clears the refs array and the suggestions state variable array.
        this.state.suggestions.length = 0, this.search_refs.length = 0
    }

    selectOption(click=false,suggestion=null){  //sets the value of the search box as the suggestions text.        
        click   
            ? this.refs.search.value = suggestion.textContent //if this function is triggered by mouse click on suggestion.
            : this.search_refs.forEach(suggestion => {    //if triggered by Enter key or Right Arrow key.  
                if(RegExp('highlight').test(suggestion.className)) this.refs.search.value = suggestion.textContent })
        this.clearHighlighted()    //clears all the highlighted suggestions.
    }

    autoCompleteResults(e){     //finds all the auto-complete suggestions that will be displayed.
        let search = e.target.value
        this.clearArrays()     //clears the suggestion and refs arrays.
        if(search.length > 1){      //if the search term is greater than 1 character.
            let count = 0      //set the count to 0.
            let temp_results = []      //temporary array to hold the newly found suggestions/results.
            try{
                //let reg = RegExp(search)    //create the regular expression.
                this.state.locations.forEach(location => {  //search for suggestions.
                    if(RegExp(search).test(location)){      //if there is a regex match.
                        count++    //increment the counter.
                        this.state.suggestions.push(    //push the formatted suggestion into the suggestions state variable.
                            <div key={count} id={'suggestion-' + count} tabIndex='-1' 
                                role='option' aria-selected='false'
                                className='suggestion' onClick={this.mouseClick}
                                ref={elem => temp_results.push(elem)}>
                                <div className='suggestion-text'>{location}</div>
                            </div>
                        )
                    }
                })
                this.search_refs = temp_results    //sets the refs for each suggestion into an array.
            }catch(err){/*console.log('could not complete search.')*/}
            this.state.suggestions.length > 0   //sets the announcement message if there are any suggestions.
                ? count += ' suggestions found, use the up and down arrows to review' 
                : count = null
            this.state.counter = count
        }else if(search.length < 2) this.state.counter = null    //if the search term is less than 2 characters dont display anything.   
        this.setSuggestions(this.state.suggestions, this.state.counter)  //sets the state for the 2 given state variables.
    }

    renderResults(results=null){    //renders the auto-complete suggestions to the page.
        if(this.state.suggestions.length != 0)
            results = <div id='results' role='listbox' tabIndex='-1'>{this.state.suggestions}</div>
        return results
    }

    setSuggestions(suggestion_list, count){      //keeps all setState calls in one place. - sets state of suggestions and counter.
        this.setState({ suggestions: suggestion_list, counter: count })
    }

    render() {
        return (
            <div>
                <h3>Accessible Auto-Suggest</h3>
                <div id='search-announcement' aria-live='assertive'>{this.state.counter}</div>
                <div id='search-field'>
                    <form>
                        <label htmlFor='search'>Suburb Search</label>
                        <div className='search-box'>
                            <input id='search' type='text' autoComplete='off'
                                role='combobox' aria-autocomplete='both' ref='search'
                                aria-owns='results' onChange={this.autoCompleteResults}
                                onKeyDown={this.keyPress}/>
                        </div>
                    </form>   
                </div>
                <div id='search-results'>{this.renderResults()}</div>                
            </div>
        )
    }
}

export default AutoCompleteComponent